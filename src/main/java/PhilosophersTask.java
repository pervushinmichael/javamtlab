import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class PhilosophersTask {

    public static void main(String[] args) {
        Food food = new Food();
        List<Philosopher> philosophers = new ArrayList<>();
        philosophers.add(new Philosopher("Сократ"));
        philosophers.add(new Philosopher("Платон"));
        philosophers.add(new Philosopher("Аристотель"));
        philosophers.add(new Philosopher("Фалес"));
        philosophers.add(new Philosopher("Пифагор"));

        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for(Philosopher philosopher:philosophers) executorService.submit(new Runnable() {
            @Override
            public void run() {
                philosopher.eat(food);
            }
        });

        executorService.shutdown();
        try {
            executorService.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class Philosopher {
    private final String name;

    public Philosopher(String name) {
        this.name = name;
    }

    public void eat(Food food){
        food.take(this.name);
    }
}

class Food {
    private final Semaphore semaphore = new Semaphore(2);

    public void take(String philosopherName){
        try {
            semaphore.acquire();
            System.out.println("Философ " + philosopherName + " садится за стол");
            Thread.sleep(1000);
            System.out.println((philosopherName + " поел и выходит из-за стола"));
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
