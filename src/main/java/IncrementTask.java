import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class IncrementTask {
    public static void main(String[] args) throws InterruptedException {
        MyClass myClass = new MyClass();
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        for (int i = 1; i <= 100; i++) {
            int finalI = i * 2;
            Thread incThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    myClass.addTask(finalI);
                }
            });
            executorService.submit(incThread);
        }

        executorService.shutdown();
        executorService.awaitTermination(1,TimeUnit.DAYS);
        myClass.run();
        myClass.show();
    }
}

class MyClass {
    private int a;
    private BlockingQueue<Integer> tasks;

    public MyClass() {
        a = 0;
        tasks = new ArrayBlockingQueue<>(100);
    }

    public void addTask(int task) {
        try {
            tasks.put(task);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (tasks.size() != 0) {
            int a = 0;
            try {
                a = tasks.take();
                increment(a);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    void increment(int delay) {
        int b = a;
        try {
            Thread.sleep(delay);
            a = b + 1;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "a = " + a;
    }

    void show() {
        System.out.println(this.toString());
    }
}

